// Ionic  App

// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('nonProfMobile', ['ionic', 'nonProfMobile.controllers', 'nonProfMobile.services', 'nonProfMobile.filters'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

//config for the auth token header
.config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sidebar.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl',
        resolve: {
          events: ['calendarService', function(calendarService) {
            return calendarService.getCalendarEvents();
          }],
          preferences: ['preferenceFactory', function(preferenceFactory) {
            return preferenceFactory.getMyPeferences();
          }]
        }
      }
    }
  })

  .state('app.aboutus', {
      url: '/aboutus',
      views: {
        'menuContent': {
          templateUrl: 'templates/aboutus.html',
          controller: 'AboutUsCtrl',
          resolve: {
            leaders: ['leadershipFactory', function(leadershipFactory) {
              return leadershipFactory.query();
            }]
          }
        }
      }
    })

  .state('app.eventcalendar', {
    url: '/events',
    params : {
      fromFeed: false //check if subscribed events must auto show
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/eventcalendar.html',
        controller: 'EventCtrl',
        resolve: {
          events: ['calendarService', function(calendarService) {
            return calendarService.getCalendarEvents();
          }],
          preferences: ['preferenceFactory', function(preferenceFactory) {
            return preferenceFactory.getMyPeferences();
          }],
          openWithSubscribedEvents: ['$stateParams', function($stateParams) {
            return $stateParams.fromFeed;
          }]
        }
      }
    }
  })

  .state('app.eventdetails', {
    url: '/events/:eventId',
    views: {
      'menuContent': {
        templateUrl: 'templates/eventdetails.html',
        controller: 'EventDetailCtrl',
        resolve: {
          event: ['$stateParams', 'calendarService', function($stateParams, calendarService) {
            return calendarService.getCalendarEvent($stateParams.eventId);
          }]
        }
      }
    }
  })

  .state('app.gallery', {
    url: '/gallery',
    views: {
      'menuContent': {
        templateUrl: 'templates/gallery.html',
        controller: 'GalleryCtrl'
      }
    }
  })

  .state('app.gallerydetails', {
    url: '/gallery/:galleryId',
    views: {
      'menuContent': {
        templateUrl: 'templates/gallerydetails.html',
        controller: 'GalleryDetailCtrl'
      }
    }
  })

  .state('app.contactus', {
    url: '/contactus',
    views: {
      'menuContent': {
        templateUrl: 'templates/contactus.html'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
