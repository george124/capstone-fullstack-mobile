angular.module('nonProfMobile.controllers', [])

.controller('AppCtrl', ["$scope", "$ionicModal", "$timeout", "AuthService", 'preferenceFactory',
function($scope, $ionicModal, $timeout, AuthService, preferenceFactory) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  //check if user is authenticated
  $scope.isAuthenticated = AuthService.isAuthenticated();

  //register for login/logout event to update views
  $scope.$on('update:Controllers', function () {
    $scope.isAuthenticated = AuthService.isAuthenticated();
  });

  // Form data for the login modal
  $scope.loginData = {};
  $scope.loginErr = false;

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login');

    //log the user in then close if Successful
    //the first parameter is the err from register (ignore)
    //second parameter is the err from login
    //third parameter is the response data (ignore)
    AuthService.login($scope.loginData, function(regErr, logErr, resp){
      if (logErr) {
        $scope.loginErr = true;
      } else {
        $scope.loginErr = false;
        $scope.closeLogin();
      }
    });
  };

  //form data form register
  $scope.registerData = {};
  $scope.registerErr = false;
  $scope.registerErrMsg = "";

  // Create the registration modal that we will use later
  $ionicModal.fromTemplateUrl('templates/register.html', {
      scope: $scope
  }).then(function (modal) {
      $scope.registerform = modal;
  });

  // Triggered in the registration modal to close it
  $scope.closeRegister = function () {
      $scope.registerform.hide();
  };

  // Open the registration modal
  $scope.register = function () {
      $scope.registerform.show();
  };

  // Perform the registration action when the user submits the registration form
  $scope.doRegister = function () {
      console.log('Doing reservation', $scope.registerData);

      //the first parameter is the err from register
      //second parameter is the err from login
      //third parameter is the response data (ignore)
      AuthService.register($scope.registerData, AuthService.login, function (regErr, logErr, resp) {
        if (regErr) {
          $scope.registerErr = true;
          $scope.registerErrMsg = "Registration was successful. But an error occurred logging you in. Please try to login again later.";
        } else if (logErr) {
          $scope.registerErr = true;
          $scope.registerErrMsg = "There was an error during registration. Please try again alater.";
        } else {
          //registration success
          //user will also be logged in since login function is called
          //after successful registrtion
          $scope.registerErr = false;
          $scope.registration = {}; //reset
          $scope.closeRegister();
        }
      });
  };

  //form data for settings
  $scope.settingsData = {};
  $scope.settingsErr = false;

  // Create the registration modal that we will use later
  $ionicModal.fromTemplateUrl('templates/settings.html', {
      scope: $scope
  }).then(function (modal) {
      $scope.settingsform = modal;
  });

  // Triggered in the registration modal to close it
  $scope.closeSettings = function () {
      $scope.settingsform.hide();
  };

  // Open the registration modal
  $scope.settings = function () {
    preferenceFactory.getMyPeferences(function(err, data) {
      if (!err) { $scope.settingsData = data; $scope.settingsErr = false; }
      else { $scope.settingsErr = true; }
      $scope.settingsform.show();
    });
  };

  // Updates the users settings when successful
  $scope.doUpdateSettings = function () {
      console.log('Updating settings');

      preferenceFactory.updateMyPeferences($scope.settingsData, function (err, result) {
        if (err) { $scope.settingsErr = true; }
        else {
          //update the view since preference has changed
          $scope.$parent.$broadcast('update:Controllers');
          $scope.closeSettings();
        }
      });
  };

  // Logs the user out
  $scope.doLogout = function () {
      console.log('Doing logout');

      // Calls auth factory to log the user outreach
      AuthService.logout(function() {
        $scope.closeSettings();
      });
  };
}])

.controller('HomeCtrl', ['$scope', 'events', 'preferences', 'calendarService', 'AuthService', 'preferenceFactory',
function($scope, events, preferences, calendarService, AuthService, preferenceFactory) {
  var processEventsFeed = function() {
    var fullFeed = [];
    //check if user is loggen in and show customized feed
    if (AuthService.isAuthenticated()){
      $scope.showNotLoggedInMessage = false;
      fullFeed = calendarService.getPreferedEvents(preferences, events);
    } else {
      $scope.showNotLoggedInMessage = true;
      fullFeed = events;
    }

    $scope.eventFeed = calendarService.getCurrentEvents(fullFeed).splice(0,3);//only show 3
    $scope.eventFeedAmout = fullFeed.length;

    //check if there are upcoming events available
    if ($scope.eventFeed.length > 0) { $scope.feedAvailable = true; }
    else { $scope.feedAvailable = false; }
  };

  var processFeaturedEvent = function() {
    //get the most current featured event
    $scope.featuredEvent = calendarService.getFeaturedEvents(calendarService.getCurrentEvents(events))[0];
    $scope.isFeaturedEventAvailable = $scope.featuredEvent;
  };

  var processNextGeneralMeeting = function() {
    $scope.nextGeneralMeeting = calendarService.findEvents(
      //first get the current events, then filter them
      calendarService.getCurrentEvents(events),
      //search values
      {
        category: 'Meeting',
        title: 'General'
      }
    )[0];

    //check if there is a meeting available
    $scope.meetingAvailable = $scope.nextGeneralMeeting !== undefined;
  };

  var processMostRecentGallery = function() {
    $scope.recentGallery = {};
  };

  //process the info for home page
  processEventsFeed();
  processFeaturedEvent();
  processNextGeneralMeeting();
  processMostRecentGallery();

  //register for login/logout event to update views
  $scope.$on('update:Controllers', function (event, data) {
    preferenceFactory.getMyPeferences(function(err, data) {
      if (!err) { preferences = data; }
      processEventsFeed();
    });
  });
  //fires when this controller loads for the first time
  $scope.$emit('controllers:Loaded');
}])

.controller('AboutUsCtrl', ['$scope', 'leaders', function($scope, leaders) {
  // leaders.sort(function(a, b) {
  //   return a.rank - b.rank;
  // });
  $scope.leaders = leaders;
}])

.controller('EventCtrl', ['$scope', 'events', 'preferences', 'calendarService', 'AuthService', 'openWithSubscribedEvents',
function($scope, events, preferences, calendarService, AuthService, openWithSubscribedEvents) {
  //search
  $scope.showSearch = false;
  $scope.search = {text: ""};
  //category selection
  $scope.categoryOptions = [
    {id: null, name: 'All'},
    {id: 'Social', name: 'Socials'},
    {id: 'Meeting', name: 'Meetings'},
    {id: 'Fundraiser', name: 'Fundraisers'},
    {id: 'Outreach', name: 'Outreach'}
  ];
  $scope.selectedCatergory = { value: $scope.categoryOptions[0] };
  //subscribed events
  $scope.subscription = {
    showSubscriptions: false,
    text: "",
    disabled: true
  };

  $scope.toggleSearch = function() {
    $scope.showSearch = !$scope.showSearch;
    //clear search values on close
    if ($scope.search.text.length > 0) {
      $scope.search.text = "";
    }
  };

  $scope.toggleSubscription = function() {
    //filter the event based on preference
    if ($scope.subscription.showSubscriptions) {
      $scope.events = calendarService.getPreferedEvents(preferences, events);
    } else {
      $scope.events = events;
    }
  };

  //check if user is loggen in
  var processSubscribedEvents = function() {
    if (AuthService.isAuthenticated()){
      $scope.subscription.disabled = false;
      $scope.subscription.text = "Only show my subscribed events";

      //if users open from event feed then automatically
      //show events they are subecribed to
      $scope.subscription.showSubscriptions = openWithSubscribedEvents;
    } else {
      $scope.subscription.disabled = true;
      $scope.subscription.text = "Login to see your subscribed events";

      //reset full calendar if needs be
      if ($scope.subscription.showSubscriptions) {
        $scope.subscription.showSubscriptions = !$scope.subscription.showSubscriptions;
        $scope.toggleSubscription();
      }
    }
  };

  $scope.events = events;
  processSubscribedEvents();

  //register for login/logout event to update views
  $scope.$on('update:Controllers', function () {
    processSubscribedEvents();
  });
}])

.controller('EventDetailCtrl', ['$scope', 'event', function($scope, event) {
  $scope.event = event;

  $scope.selected = 1;

  $scope.selectTab = function(tab) {
    $scope.selected = tab;
  };

  $scope.isSelected = function(tab) {
    return tab === $scope.selected;
  }
}])

.controller('GalleryCtrl', function($scope) {

})

.controller('GalleryDetailCtrl', function($scope, $stateParams) {
});
