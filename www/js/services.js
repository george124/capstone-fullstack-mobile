'use strict';

angular.module('nonProfMobile.services', ['ngResource'])
  .constant("baseURL","https://fullstack-capstone-george.herokuapp.com/api/")

  .factory('eventFactory', ['$resource', 'baseURL', function($resource, baseURL) {
    return $resource(baseURL + "events/:eventId", { eventId: '@_id' });
  }])

  .factory('galleryFactory', ['$resource', 'baseURL', function($resource, baseURL) {
    return $resource(baseURL + "events/:eventId/gallery/:galleryId", { eventId: '@_id', galleryId: '@galleryId' }, {
      'update': {
        method: 'PUT'
      }
    });
  }])

  .factory('leadershipFactory', ['$resource', 'baseURL', function($resource, baseURL) {
    return $resource(baseURL + "leadership", null);
  }])

  .factory('rsvpFactory', ['$resource', 'baseURL', function($resource, baseURL) {
    return $resource(baseURL + "rsvp/:eventId", { eventId: '@_id' }, {
      'update': {
        method: 'PUT'
      }
    });
  }])

  .factory('preferenceFactory', ['$resource', 'baseURL', function($resource, baseURL) {
    var prefFact = {};

    var prefResource = $resource(baseURL + "users/settings", {}, {
      'update': {
        method: 'PUT'
      }
    });

    prefFact.getMyPeferences = function(cb) {
      if (!cb) { return prefResource.get({}); }
      else {
        prefResource.get({}, function(data) {
          cb(null, data);
        },
        function(err) {
          cb(err, null);
        });
      }
    };

    prefFact.updateMyPeferences = function(prefs, cb) {
      prefResource.update({}, prefs, function(data) {
        cb(null, data);
      },
      function(err) {console.log("err", err);
        cb(err, null);
      });
    };

    return prefFact;
  }])

  .factory('AuthFactory', ['$localStorage', function($localStorage){
    var authFac = {};
    var TOKEN_KEY = 'Token';
    var username = '';
    var authToken = undefined;

    authFac.loadUserCredentials = function() {
      var credentials = $localStorage.getObject(TOKEN_KEY,'{}');
      if (credentials.username != undefined) {
        useCredentials(credentials);
        //return true, user is authenticated
        return true;
      }
      //user not authenticated
      return false;
    }

    authFac.storeUserCredentials = function(credentials) {
      $localStorage.storeObject(TOKEN_KEY, credentials);
      useCredentials(credentials);
    }

    function useCredentials(credentials) {
      username = credentials.username || '';
      authToken = credentials.token;

      // Set the token as header for your requests!
      // $http.defaults.headers.common['x-access-token'] = authToken;
    }

    authFac.destroyUserCredentials = function() {
      authFac.storeUserCredentials({authToken: undefined, username: undefined
      });
      // $http.defaults.headers.common['x-access-token'] = authToken;
      $localStorage.remove(TOKEN_KEY);
    }

    authFac.getAuthToken = function() {
      return authToken;
    };

    authFac.getUsername = function() {
        return username;
    };

    return authFac;
  }])

  .service('AuthService', ['$resource', '$localStorage', '$rootScope', 'baseURL', 'AuthFactory',
  function($resource, $localStorage, $rootScope, baseURL, AuthFactory){
    var isAuthenticated = false;

    var authListener = $rootScope.$on('controllers:Loaded', function() {
      //load the credentials
      isAuthenticated = AuthFactory.loadUserCredentials();
      if (isAuthenticated) { $rootScope.$broadcast('update:Controllers'); }
      authListener(); //stop listening
    });

    this.login = function(loginData, cb) {
      $resource(baseURL + "users/login")
      .save(loginData,
         function(response) {
            AuthFactory.storeUserCredentials({username:loginData.username, token: response.token});
            isAuthenticated = true;
            $rootScope.$broadcast('update:Controllers');

            //run the callback
            cb(null, null, response);
         },
         function(response){
            isAuthenticated = false;

            //call the cb function with the Error
            //pass null to first arg (used for register err which we ignore here)
            cb(null, response.data.err, null);
         }
      );
    };

    this.logout = function(cb) {
      $resource(baseURL + "users/logout").get(function(response){
      });
      AuthFactory.destroyUserCredentials();
      isAuthenticated = false;
      $rootScope.$broadcast('update:Controllers');

      //run the callback
      cb();
    };

    this.register = function(registerData, login, cb) {
      $resource(baseURL + "users/register")
      .save(registerData,
         function(response) {
          login({username:registerData.username, password:registerData.password}, cb);
          //we are on mobile so store user ionicPlatform
          $localStorage.storeObject('userinfo',
              {username:registerData.username, password:registerData.password});
          $rootScope.$broadcast('registration:Successful');
         },
         function(response){
           //call the cb function with the Error
           //pass null to second arg (used for login err which we ignore here)
           cb(response.data.err, null, null);
         }
      );
    };

    this.isAuthenticated = function() {
        return isAuthenticated;
    };

    //register for when relogin is needed, then log user in
    var reLoginListener = $rootScope.$on('relogin:Needed', function () {
      console.log("relogin recieved");
    });
    //destroy scopes
    var destroyScopeListener = $rootScope.$on('$destroy', function(){
      reLoginListener();
      destroyScopeListener();
    });
  }])

  .service('calendarService', ['$sce', '$q', 'eventFactory',
  function($sce, $q, eventFactory){
    //helper function to get the default image based on the event type
    function getDefaultImage(eventType){
      switch (eventType) {
        case "Meeting":
          return "https://dl.dropboxusercontent.com/u/98508428/CAM-images/events.jpg";
        case "Fundraiser":
          return "https://dl.dropboxusercontent.com/u/98508428/CAM-images/fundraisers.jpg";
        case "Social":
          return "https://dl.dropboxusercontent.com/u/98508428/CAM-images/socials.jpg";
        case "Outreach":
          return "https://dl.dropboxusercontent.com/u/98508428/CAM-images/outreach.jpg";
      }
    }

    //helper function to break up the address by comma(,) and add a<br/>
    //returns the new formatted address
    function processAddressAsHtml(address){
      var addressArr = address.split("");
      //replace each comma with <br/>
      addressArr = addressArr.map(function(val){
        if (val === ","){
          val = ",<br/>";
        }
        return val;
      });
      //convert the array to string
      //process it as html and finally
      //return it
      return $sce.trustAsHtml(addressArr.join(""));
    }

    //function to sort the event data by descending dates
    //most upcoming events first then by newest to older
    function sortByDateDescending(data){
      data.sort(function(a, b){
        //return <1, a b4 b
        //return >1, b b4 a
        //otherwise return 0
        var curTime = new Date().getTime();
        a = curTime - new Date(a.dateAndTime).getTime();
        b = curTime - new Date(b.dateAndTime).getTime();

        if (a < 0 && b < 0){
          //both in future then return one closest
          return b - a;
        }
        else if (a > 0 && b > 0){
          //both in past then return one closest
          return a - b;
        } else {
          //one in past
          return a - b;
        }
      });
      return data;
    }

    function formatEvent(eventItem){
      //reconstruct the data keeping only the elements we need
      //if the event item has imageUrl800px450px, firstItemLink,
      //and or secondItemLink then customize it for viewing/downloading
      if (!eventItem.hasOwnProperty('image') || eventItem.image === ""){
        //get the default image for the event category
        eventItem.image = getDefaultImage(eventItem.category);
      }

      if (eventItem.firstItem.link){
        eventItem.showFirstLink = true;
      } else { eventItem.showFirstLink = false; }

      if (eventItem.secondItem.link){
        eventItem.showSecondLink = true;
      } else { eventItem.showSecondLink = false; }

      //check for duration of event
      if (eventItem.durationHrs > 0){
        eventItem.showDuration = true;
      } else { eventItem.showDuration = false; }

      //process the address for optimal viewing
      eventItem.formattedAddress = processAddressAsHtml(eventItem.address);

      //check for description of event
      if (eventItem.hasOwnProperty('description')){
        eventItem.showMessage = true;
      } else { eventItem.showMessage = false; }

      //check for admission info
      if (eventItem.cost && eventItem.cost.length > 0) {
        eventItem.showAdmission = true;
      } else { eventItem.showAdmission = false; }

      return eventItem;
    }

    this.getCalendarEvents = function() {
      var deferred = $q.defer();

      //fetch the data from the server
      eventFactory.query(
        function(response) {
          response = sortByDateDescending(response);
          deferred.resolve(response.map(function(data) {
              return formatEvent(data);
            })
          );
        },
        function(response) {
            deferred.reject("Error: " + response.status + " " + response.statusText);
        }
      );

      return deferred.promise;
    };

    this.getCalendarEvent = function(findById) {
      var deferred = $q.defer();

      //fetch the data from the server
      eventFactory.get({eventId: findById},
        function(response) {
          deferred.resolve(formatEvent(response));
        },
        function(response) {
            deferred.reject("Error: " + response.status + " " + response.statusText);
        }
      );

      return deferred.promise;
    };

    this.getURLFriendlyAddress = function(address) {
      var mapObj = {' ':"+",'<b>':"",'</b>':"",'.':""};

      var re = new RegExp(Object.keys(mapObj).join("|"),"gi");
      return address.replace(re, function(matched){
        return mapObj[matched] || matched;
      });
    };

    this.getPreferedEvents = function(preferences, events) {
      //filters the events based on the set event showSubscriptions
      //preferences
      return events.filter(function(val) {
        if ((preferences.subscribeOutreach && val.category === 'Outreach') ||
        (preferences.subscribeMeetings && val.category === 'Meeting') ||
        (preferences.subscribeSocials && val.category === 'Social') ||
        (preferences.subscribeFundraisers && val.category === 'Fundraiser')) {
          return true;
        }

        return false;
      });
    };

    this.getCurrentEvents = function(events) {
      //filters the events based on the date (current)
      var yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1); //use yesterday's date for extra 24 hours
      yesterday = yesterday.getTime();
      return events.filter(function(val) {
        return new Date(val.date).getTime() > yesterday;
      });
    };

    this.getFeaturedEvents = function(events) {
      //filters the events based on if they are featrued or not
      return events.filter(function(val) {
        return val.isFeatured;
      });
    };

    this.findEvents = function(events, searchTerms) {
      //check if search terms are empty
      if (!searchTerms || (!searchTerms.category && !searchTerms.title)) { return events; }

      var titleRegEx = ''; //always return true
      var categoryRegEx = '';//always return true

      //set up the regex based on search terms
      if (searchTerms.category) { categoryRegEx = new RegExp(searchTerms.category, "gi"); }
      if (searchTerms.title) { titleRegEx = new RegExp(searchTerms.title, "gi"); }

      return events.filter(function(val) {
        return val.category.search(categoryRegEx) > -1 && val.name.search(titleRegEx) > -1;
      });
    };
  }])

  .factory('$localStorage', ['$window', function($window) {
    return {
      store: function(key, value) {
        $window.localStorage[key] = value;
      },
      get: function(key, defaultValue) {
        return $window.localStorage[key] || defaultValue;
      },
      storeObject: function(key, value) {
        $window.localStorage[key] = JSON.stringify(value);
      },
      getObject: function(key, defaultValue) {
        return JSON.parse($window.localStorage[key] || defaultValue);
      },
      remove: function(key) {
        if (!key || !$window.localStorage.hasOwnProperty(key)) { return; }
        delete $window.localStorage.key;
      }
    }
  }])

  .factory('authInterceptor', function ($q, AuthFactory, $rootScope) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if (AuthFactory.getAuthToken()) {
          config.headers['x-access-token'] = AuthFactory.getAuthToken();
        }
        return config;
      },
      response: function (response) {
        if (response.status === 401) {console.log(response);
          // handle the case where the user is not authenticated
          $rootScope.$emit('relogin:Needed');console.log("relogin needed");
        }
        return response || $q.when(response);
      }
    };
  })

;
