'use strict';

angular.module('nonProfMobile.filters', [])

  .filter('trunc', function(){
    //truncate the given string by the given length
     return function(input, len){
       //only truncate string by the given length
       if(!angular.isString(input)) { return; }

       if (input.length > len) {
         input = input.substr(0,len - 3) + "...";
       }
       return input;
     };
  })

  .filter('paginate', function(){
    //paginate an array by page number and items per page
    return function(array, pageNumber, itemsPerPage) {
      if (!array || array.length ===0) { return; }

      var begin = ((pageNumber - 1) * itemsPerPage);
      var end = begin + itemsPerPage;
      return array.slice(begin, end);
    };
  })

  .filter('eMoney', function(){
    //format the given input as (currency) 2 decimal places
    //if value === 0 then return Free
     return function(input){
       if (typeof input !== "number"){
         return;
       }else if (input === 0){
         return "Free";
       }else {
         return "$" + input.toFixed(2);
       }
     };
  });
